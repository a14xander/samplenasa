package com.sample.samplenasa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.sample.samplenasa.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initNavigation()
    }

    private fun initNavigation() {
        navController = findNavController(R.id.navHostFragment)
        binding.apply {
            bottomNavView.apply {
                setupWithNavController(navController)
                setOnItemSelectedListener {
                    navController.apply {
                        popBackStack()
                        navigate(it.itemId)
                    }
                    return@setOnItemSelectedListener true
                }
                visibility = View.VISIBLE
            }
        }
    }
}