package com.sample.samplenasa.ui.screens.patents

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.sample.samplenasa.R
import com.sample.samplenasa.data.model.responses.patents.PatentsModel
import com.sample.samplenasa.databinding.FragmentPatentsBinding
import com.sample.samplenasa.ui.adapters.patents.PatentsAdapter
import com.sample.samplenasa.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class PatentsFragment : BaseFragment() {

    private val patentsViewModel by viewModel<PatentsViewModel>()
    private lateinit var binding: FragmentPatentsBinding
    private val patentsAdapter = PatentsAdapter()

    override fun screenDescription() = getString(R.string.patents_screen_description)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPatentsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun initViews() {
        binding.apply {
            rvPatents.apply {
                adapter = patentsAdapter
                layoutManager = LinearLayoutManager(context)
            }
            srlPatents.setOnRefreshListener { patentsViewModel.requestPatentData() }
        }
    }

    private fun stopUpdating() {
        binding.apply {
            srlPatents.isRefreshing = false
            pbPatents.isVisible = false
        }
    }

    override fun subscribeUI() {
        viewLifecycleOwner.let {
            patentsViewModel.apply {
                errorMessage.observe(it, { processErrorMessage(it) })
                patentsList.observe(it, { processPatentsList(it) })
            }
        }
    }

    private fun processPatentsList(list: List<PatentsModel>?) {
        stopUpdating()
        binding.rvPatents.isVisible = true
        list?.let { patentsAdapter.updatePatentsList(it) }
    }

    private fun processErrorMessage(message: String?) {
        stopUpdating()
        message?.let { Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show() }
    }
}