package com.sample.samplenasa.ui.adapters.apod

import android.view.LayoutInflater
import android.view.ViewGroup
import android.webkit.WebView.HitTestResult.UNKNOWN_TYPE
import androidx.recyclerview.widget.RecyclerView
import com.sample.samplenasa.R
import com.sample.samplenasa.data.model.responses.apod.Apod
import com.sample.samplenasa.utils.TYPE_IMAGE
import com.sample.samplenasa.utils.TYPE_LOADING
import com.sample.samplenasa.utils.TYPE_UNKNOWN
import com.sample.samplenasa.utils.TYPE_VIDEO
import kotlinx.coroutines.channels.BroadcastChannel
import java.nio.channels.Channel

class ApodAdapter(private val channel: BroadcastChannel<Pair<String, String>>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoaderVisible = false
    private var apodList: MutableList<Apod?> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val apodItem = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.viewholder_apod_item, parent, false)
        return when (viewType) {
            TYPE_IMAGE -> ApodImageViewHolder(apodItem)
            TYPE_VIDEO -> ApodVideoViewHolder(apodItem)
            TYPE_LOADING ->
                ApodProgressViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.viewholder_loading, parent, false)
                )
            else ->
                ApodProgressViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.viewholder_empty, parent, false)
                )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item: Apod? = apodList[position]
        when (holder) {
            is ApodImageViewHolder -> holder.onBind(item)
            is ApodVideoViewHolder -> holder.onBind(item, channel)
        }
    }

    override fun getItemCount() = apodList.size

    override fun getItemViewType(position: Int): Int {
        apodList.apply {
            val item = this[position]
            return if (isLoaderVisible) {
                if (position == size - 1) TYPE_LOADING
                else item?.getItemType() ?: UNKNOWN_TYPE
            } else item?.getItemType() ?: UNKNOWN_TYPE
        }
    }

    fun addItems(apodItems: List<Apod>?) {
        apodItems?.let {
            apodList.addAll(it)
            notifyDataSetChanged()
        }
    }

    fun addLoading() {
        isLoaderVisible = true
        apodList.apply {
            add(null)
            notifyItemInserted(size - 1)
        }
    }

    fun clearApodList() {
        apodList = mutableListOf()
    }

    fun removeLoading() {
        isLoaderVisible = false
        apodList.apply {
            val position = size - 1
            val item: Apod? = this[position]
            if (item == null) {
                removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }
}