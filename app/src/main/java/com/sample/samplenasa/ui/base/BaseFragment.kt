package com.sample.samplenasa.ui.base

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.sample.samplenasa.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.scope.ScopeFragment

abstract class BaseFragment : ScopeFragment() {
    open fun hasBottomNavigation() = true
    protected abstract fun initViews()
    protected abstract fun subscribeUI()
    protected abstract fun screenDescription(): String

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        isBottomNavigationVisible(hasBottomNavigation())
    }

    private fun isBottomNavigationVisible(isVisible: Boolean) {
        (requireActivity() as MainActivity)
            .bottomNavView.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun showToast() {
        Toast.makeText(activity, screenDescription(), Toast.LENGTH_LONG).show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeUI()
        initViews()
        showToast()
    }
}