package com.sample.samplenasa.ui.adapters.apod

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.sample.samplenasa.data.model.responses.apod.Apod

class ApodProgressViewHolder(baseView: View) : RecyclerView.ViewHolder(baseView) {

    fun onBind(apod: Apod?) {}
}