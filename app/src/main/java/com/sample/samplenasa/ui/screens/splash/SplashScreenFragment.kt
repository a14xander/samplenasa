package com.sample.samplenasa.ui.screens.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.sample.samplenasa.R
import com.sample.samplenasa.databinding.FragmentSplashScreenBinding
import com.sample.samplenasa.ui.base.BaseFragment
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashScreenFragment : BaseFragment() {

    private val splashScreenViewModel by viewModel<SplashScreenViewModel>()
    private lateinit var binding: FragmentSplashScreenBinding

    override fun hasBottomNavigation() = false
    override fun initViews() {}
    override fun subscribeUI() {}
    override fun screenDescription() = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashScreenBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GlobalScope.launch {
            delay(1000) //TODO make first apod request
            withContext(Main) {
                val direction = SplashScreenFragmentDirections.actionSplashScreenFragmentToApodFragment()
                findNavController().navigate(direction)
            }
        }
    }
}