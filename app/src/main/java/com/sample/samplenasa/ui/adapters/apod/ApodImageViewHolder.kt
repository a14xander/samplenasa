package com.sample.samplenasa.ui.adapters.apod

import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sample.samplenasa.data.model.responses.apod.Apod
import com.sample.samplenasa.databinding.ViewholderApodItemBinding

class ApodImageViewHolder(private val baseView: View) : RecyclerView.ViewHolder(baseView) {
    private val binding = ViewholderApodItemBinding.bind(baseView)

    fun onBind(apod: Apod?) {
        apod?.let {
            binding.apply {
                tvApodTitle.text = it.title
                tvApodDescription.text = it.explanation
                incApodImg.root.isVisible = true

                Glide
                    .with(baseView.context)
                    .load(it.url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .into(incApodImg.ivApod)
            }
        }
    }
}