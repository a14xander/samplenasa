package com.sample.samplenasa.ui.screens.info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sample.samplenasa.BuildConfig
import com.sample.samplenasa.R
import com.sample.samplenasa.databinding.FragmentInfoBinding
import com.sample.samplenasa.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class InfoFragment : BaseFragment() {

    private lateinit var binding: FragmentInfoBinding
    private val infoViewModel by viewModel<InfoViewModel>()

    override fun screenDescription() = ""
    override fun subscribeUI() {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentInfoBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun initViews() {
        binding.tvApplicationVersion.text =
            String.format(getString(R.string.application_version), BuildConfig.VERSION_NAME)
    }
}