package com.sample.samplenasa.ui.screens.fireball

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.sample.samplenasa.R
import com.sample.samplenasa.data.model.responses.fireball.FireballModel
import com.sample.samplenasa.databinding.FragmentFireballBinding
import com.sample.samplenasa.ui.adapters.fireball.FireballAdapter
import com.sample.samplenasa.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class FireballFragment : BaseFragment() {

    private val fireballViewModel by viewModel<FireballViewModel>()
    private lateinit var binding: FragmentFireballBinding
    private val fireballAdapter = FireballAdapter()

    override fun screenDescription() = getString(R.string.fireball_screen_description)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFireballBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun subscribeUI() {
        viewLifecycleOwner.let {
            fireballViewModel.apply {
                fireballList.observe(it, { processFireballList(it) })
                errorMessage.observe(it, { processErrorMessage(it) })
            }
        }
    }

    private fun processErrorMessage(message: String?) {
        stopUpdating()
        message?.let { Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show() }
    }

    private fun processFireballList(list: List<FireballModel>?) {
        stopUpdating()
        binding.rvFireball.isVisible = true
        list?.let { fireballAdapter.updateFireballList(it) }
    }

    private fun stopUpdating() {
        binding.apply {
            srlFireball.isRefreshing = false
            pbFireball.isVisible = false
        }
    }

    override fun initViews() {
        binding.apply {
            srlFireball.setOnRefreshListener { fireballViewModel.requestFireballData() }
            rvFireball.apply {
                adapter = fireballAdapter
                layoutManager = LinearLayoutManager(context)
            }
        }
    }
}