package com.sample.samplenasa.ui.adapters.fireball

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.sample.samplenasa.data.model.responses.fireball.FireballModel
import com.sample.samplenasa.databinding.ViewholderFireballBinding

class FireballViewHolder(private val baseView: View) : RecyclerView.ViewHolder(baseView) {
    private val binding = ViewholderFireballBinding.bind(baseView)

    fun onBind(fireballModel: FireballModel) {
        binding.apply {
            fireballModel.apply {
                tvLatValue.text = lat
                tvLonValue.text = lon
                tvAltValue.text = alt
                tvVelValue.text = vel
                tvDateTitle.text = date
            }
        }
    }
}