package com.sample.samplenasa.ui.adapters.fireball

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sample.samplenasa.R
import com.sample.samplenasa.data.model.responses.fireball.FireballModel

class FireballAdapter : RecyclerView.Adapter<FireballViewHolder>() {

    private var fireballList = mutableListOf<FireballModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        FireballViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.viewholder_fireball, parent, false)
        )

    override fun onBindViewHolder(holder: FireballViewHolder, position: Int) {
        holder.onBind(fireballList[position])
    }

    override fun getItemCount() = fireballList.size

    fun updateFireballList(fireballList: List<FireballModel>) {
        this.fireballList = fireballList.toMutableList()
        notifyDataSetChanged()
    }
}