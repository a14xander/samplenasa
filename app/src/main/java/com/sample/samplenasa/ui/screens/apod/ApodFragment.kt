package com.sample.samplenasa.ui.screens.apod

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.sample.samplenasa.R
import com.sample.samplenasa.data.model.responses.apod.Apod
import com.sample.samplenasa.databinding.FragmentApodBinding
import com.sample.samplenasa.ui.adapters.apod.ApodAdapter
import com.sample.samplenasa.ui.base.BaseFragment
import com.sample.samplenasa.utils.PaginationListener
import kotlinx.coroutines.channels.BroadcastChannel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ApodFragment : BaseFragment() {

    private val apodViewModel by viewModel<ApodViewModel>()
    private lateinit var binding: FragmentApodBinding
    private val channel = BroadcastChannel<Pair<String,String>>(1)
    private val apodAdapter = ApodAdapter(channel)

    override fun screenDescription() = getString(R.string.apod_screen_description)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentApodBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun initViews() {
        apodViewModel.apply {
            setInitialDate()
            initChannel(channel)
        }

        binding.apply {
            rvApod.apply {
                adapter = apodAdapter
                val manager = LinearLayoutManager(context)
                layoutManager = manager
                addOnScrollListener(object :
                    PaginationListener(manager) {
                    override fun loadMoreItems() {
                        apodViewModel.apply {
                            fetchApodData()
                            isLoading = true
                            currentPage++
                        }
                    }

                    override fun isLastPage(): Boolean = apodViewModel.isLastPage
                    override fun isLoading(): Boolean = apodViewModel.isLoading
                })
            }
        }
    }

    override fun subscribeUI() {
        viewLifecycleOwner.let {
            apodViewModel.apply {
                addLoading.observe(it, { processAddLoading(it) })
                removeLoading.observe(it, { processRemoveLoading(it) })
                apods.observe(it, { processApods(it) })
                errorMessage.observe(it, { processErrorMessage(it) })
                urlPair.observe(it, { processVideoUrl(it) })
            }
        }

    }

    private fun processVideoUrl(url: Pair<String, String>?) {
        url?.let {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:${url.second}"))
            val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url.first))
            try {
                startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                startActivity(webIntent)
            }
        }
    }

    private fun processApods(list: List<Apod>?) {
        list?.let { apodAdapter.addItems(list) }
    }

    private fun processRemoveLoading(value: Boolean?) {
        value?.let { if (it) apodAdapter.removeLoading() }
    }

    private fun processAddLoading(value: Boolean?) {
        value?.let { if (it) apodAdapter.addLoading() }
    }

    private fun processErrorMessage(message: String?) {
        message?.let { Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show() }
    }
}