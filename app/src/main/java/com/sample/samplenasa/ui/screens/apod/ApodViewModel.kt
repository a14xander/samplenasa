package com.sample.samplenasa.ui.screens.apod

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.requests.apod.ApodRequest
import com.sample.samplenasa.data.model.responses.apod.Apod
import com.sample.samplenasa.domain.repository.ApodApi
import com.sample.samplenasa.utils.PaginationListener
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.withContext


class ApodViewModel(
    private val apodRepository: ApodApi
) : ViewModel() {

    val addLoading = MutableLiveData<Boolean>()
    val removeLoading = MutableLiveData<Boolean>()
    val apods = MutableLiveData<List<Apod>>()
    val errorMessage = MutableLiveData<String>()
    val urlPair = MutableLiveData<Pair<String, String>>()

    var currentPage = PaginationListener.PAGE_START
    var isLastPage = false
    var isLoading = false
    private var totalPages = Int.MAX_VALUE
    private var observed = false

    private lateinit var channel: BroadcastChannel<Pair<String, String>>
    private lateinit var youTubeChannelJob: Job

    private var currentTimestamp: String by Delegates.observable("") { _, _, newValue ->
        if (!observed && newValue != "") {
            observed = true
            fetchApodData()
        }
    }
    private var startTimestamp: String = ""

    fun fetchApodData() {
        viewModelScope.launch {
            addLoading()
            if (currentPage >= totalPages) {
                removeLoading()
                isLastPage = true
            }

            val actualDates = getActualDates(startTimestamp, currentTimestamp)

            when (val apodResult = apodRepository.requestBatchApods(
                ApodRequest(actualDates.first, actualDates.second)
            )) {
                is Result.Success -> {
                    val apodResponse = apodResult.data.reversed()
                    apodResponse.last().date?.let {
                        val rightDate = getStartDate(it, -1)
                        startTimestamp = getStartDate(rightDate, -13)
                        currentTimestamp = it
                    }

                    if (currentPage != PaginationListener.PAGE_START) removeLoading()
                    isLoading = false

                    apods.value = apodResponse
                }
                is Result.Error -> {
                    apodResult.errorObj.error?.message?.let { errorMessage.value = it }
                }
            }
        }
    }

    fun initChannel(channel: BroadcastChannel<Pair<String, String>>) {
        this.channel = channel
        youTubeChannelJob = viewModelScope.launch {
            this@ApodViewModel.channel.consumeEach {
                withContext(Main) {
                    urlPair.value = it
                }
            }
        }
    }

    private fun formatLongToStringDate(date: Long): String {
        val date = Date(date)
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.format(date)
    }

    private fun addLoading(value: Boolean = true) {
        addLoading.value = value
    }

    private fun removeLoading(value: Boolean = true) {
        removeLoading.value = value
    }

    fun setInitialDate() {
        val endTime = "2021-09-07"//formatLongToStringDate(System.currentTimeMillis())
        startTimestamp = getStartDate(endTime, -15)
        currentTimestamp = endTime
    }

    private fun getActualDates(startDate: String, endDate: String): Pair<String, String> {
        val startDateArray = startDate.split("-")
        val endDateArray = endDate.split("-")

        val actualStartDate: String
        val actualEndDate: String

        startDateArray.apply {
            actualStartDate = "${this[0]}-${appendZero(this[1])}-${appendZero(this[2])}"
        }
        endDateArray.apply {
            actualEndDate = "${this[0]}-${appendZero(this[1])}-${appendZero(this[2])}"
        }

        return Pair(actualStartDate, actualEndDate)
    }

    private fun appendZero(number: String) =
        if (number.toInt() < 10 && !number.contains("0")) "0$number" else number

    private fun getStartDate(date: String, days: Int): String {
        val calendar = Calendar.getInstance()
        val dateArray = date.split("-")
        dateArray.apply {
            calendar.set(this[0].toInt(), this[1].toInt(), this[2].toInt())
        }
        var startDate: String
        calendar.apply {
            add(Calendar.DATE, days)
            startDate = "${get(Calendar.YEAR)}-${get(Calendar.MONTH)}-${get(Calendar.DAY_OF_MONTH)}"
        }
        return startDate
    }

    override fun onCleared() {
        super.onCleared()
        if (::youTubeChannelJob.isInitialized) youTubeChannelJob.cancel()
    }
}