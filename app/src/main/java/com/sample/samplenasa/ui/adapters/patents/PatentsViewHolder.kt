package com.sample.samplenasa.ui.adapters.patents

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.sample.samplenasa.R
import com.sample.samplenasa.data.model.responses.patents.PatentsModel
import com.sample.samplenasa.databinding.ViewholderPatentsBinding

class PatentsViewHolder(private val baseView: View) : RecyclerView.ViewHolder(baseView) {
    private val binding = ViewholderPatentsBinding.bind(baseView)

    fun onBind(patentsModel: PatentsModel) {
        binding.apply {
            patentsModel.apply {
                tvPatentTitle.text = title
                tvPatentField.text = String.format(baseView.context.getString(R.string.patent_field), scope)
                tvPatentDescription.text = description
            }
        }
    }
}