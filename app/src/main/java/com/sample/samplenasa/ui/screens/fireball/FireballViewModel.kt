package com.sample.samplenasa.ui.screens.fireball

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.fireball.FireballModel
import com.sample.samplenasa.domain.usecase.fireball.FireballTransform
import kotlinx.coroutines.launch

class FireballViewModel(
    private val fireballTransform: FireballTransform
) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val fireballList = MutableLiveData<List<FireballModel>>()

    init {
        requestFireballData()
    }

    fun requestFireballData() {
        viewModelScope.launch {
            when (val fireballResult = fireballTransform.requestFireballData()) {
                is Result.Success -> fireballList.value = fireballResult.data
                is Result.Error ->
                    fireballResult.errorObj.error?.message?.let { errorMessage.value = it }
            }
        }
    }
}