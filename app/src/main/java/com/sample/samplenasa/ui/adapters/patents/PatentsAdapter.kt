package com.sample.samplenasa.ui.adapters.patents

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sample.samplenasa.R
import com.sample.samplenasa.data.model.responses.patents.PatentsModel

class PatentsAdapter : RecyclerView.Adapter<PatentsViewHolder>() {

    private var patentList = mutableListOf<PatentsModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PatentsViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.viewholder_patents, parent, false)
        )

    override fun onBindViewHolder(holder: PatentsViewHolder, position: Int) {
        holder.onBind(patentList[position])
    }

    override fun getItemCount() = patentList.size

    fun updatePatentsList(list: List<PatentsModel>) {
        patentList = list.toMutableList()
    }
}