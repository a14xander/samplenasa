package com.sample.samplenasa.ui.adapters.apod

import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubeThumbnailLoader
import com.google.android.youtube.player.YouTubeThumbnailView
import com.sample.samplenasa.data.model.responses.apod.Apod
import com.sample.samplenasa.databinding.ViewholderApodItemBinding
import com.sample.samplenasa.utils.YOU_TUBE_API_KEY
import kotlinx.coroutines.channels.BroadcastChannel
import java.nio.channels.Channel

class ApodVideoViewHolder(private val baseView: View) : RecyclerView.ViewHolder(baseView) {
    val binding = ViewholderApodItemBinding.bind(baseView)

    fun onBind(apod: Apod?, channel: BroadcastChannel<Pair<String, String>>) {
        apod?.let {
            binding.apply {
                tvApodTitle.text = it.title
                tvApodDescription.text = it.explanation
                incApodVideo.root.isVisible = true

                val onThumbnailLoadedListener: YouTubeThumbnailLoader.OnThumbnailLoadedListener =
                    object : YouTubeThumbnailLoader.OnThumbnailLoadedListener {
                        override fun onThumbnailError(
                            youTubeThumbnailView: YouTubeThumbnailView?,
                            errorReason: YouTubeThumbnailLoader.ErrorReason?
                        ) {
                            loadGlide(apod)
                        }

                        override fun onThumbnailLoaded(
                            youTubeThumbnailView: YouTubeThumbnailView,
                            s: String?
                        ) {
                            youTubeThumbnailView.visibility = View.VISIBLE
                            incApodVideo.pbYoutube.visibility = View.GONE
                            incApodVideo.ivYoutubePlay.visibility = View.VISIBLE
                        }
                    }

                incApodVideo.ytThumbnail.initialize(YOU_TUBE_API_KEY, object :
                    YouTubeThumbnailView.OnInitializedListener {
                    override fun onInitializationSuccess(
                        youTubeThumbnailView: YouTubeThumbnailView?,
                        youTubeThumbnailLoader: YouTubeThumbnailLoader
                    ) {
                        youTubeThumbnailLoader.apply {
                            setVideo(apod.url?.let { url -> getVideoId(url) })
                            setOnThumbnailLoadedListener(onThumbnailLoadedListener)
                        }
                    }

                    override fun onInitializationFailure(
                        youTubeThumbnailView: YouTubeThumbnailView?,
                        youTubeInitializationResult: YouTubeInitializationResult?
                    ) {
                        loadGlide(it)
                    }
                })

                apod.url?.let { url ->
                    incApodVideo.ytThumbnail.setOnClickListener {
                        onClick(getVideoId(url), channel)
                    }
                    incApodVideo.ivYoutubePlay.setOnClickListener {
                        onClick(getVideoId(url), channel)
                    }
                }
            }
        }
    }

    private fun loadGlide(apod: Apod) {
        Glide.with(baseView.context)
            .load(apod.thumbnail_url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return true
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.incApodVideo.apply {
                        pbYoutube.isVisible = false
                        ivPlaceholderThumbnail.isVisible = true
                        ivYoutubePlay.isVisible = true
                    }
                    return false
                }
            })
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .fitCenter()
            .into(binding.incApodVideo.ivPlaceholderThumbnail)
    }

    private fun onClick(videoId: String, channel: BroadcastChannel<Pair<String, String>>) {
        val videoLink = String.format(urlVideoLink, videoId)
        channel.offer(Pair(videoLink, videoId))
    }

    private fun getVideoId(link: String): String {
        return link.substring(30).substringBefore('?')
    }

    companion object {
        private const val urlVideoLink = "http://www.youtube.com/watch?v=%s"
    }
}