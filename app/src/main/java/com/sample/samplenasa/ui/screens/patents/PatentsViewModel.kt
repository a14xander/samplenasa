package com.sample.samplenasa.ui.screens.patents

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.patents.PatentsModel
import com.sample.samplenasa.domain.usecase.patents.PatentsTransform
import kotlinx.coroutines.launch

class PatentsViewModel(
    private val patentsTransform: PatentsTransform
) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val patentsList = MutableLiveData<List<PatentsModel>>()

    init {
        requestPatentData()
    }

    fun requestPatentData() {
        viewModelScope.launch {
            when (val patentsResult = patentsTransform.requestPatentData()) {
                is Result.Success -> patentsList.value = patentsResult.data
                is Result.Error ->
                    patentsResult.errorObj.error?.message?.let { errorMessage.value = it }
            }
        }
    }
}