package com.sample.samplenasa.domain.repository

import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.patents.PatentsData

interface PatentsApi {

    suspend fun requestPatentData(): Result<PatentsData>
}