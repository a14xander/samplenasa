package com.sample.samplenasa.domain.repository

import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.requests.apod.ApodRequest
import com.sample.samplenasa.data.model.responses.apod.Apod

interface ApodApi {
    suspend fun requestBatchApods(apodRequest: ApodRequest): Result<List<Apod>>
}