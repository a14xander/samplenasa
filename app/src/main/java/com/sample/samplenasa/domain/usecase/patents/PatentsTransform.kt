package com.sample.samplenasa.domain.usecase.patents

import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.patents.PatentsModel
import com.sample.samplenasa.domain.repository.PatentsApi

class PatentsTransform(
    private val patentsApi: PatentsApi
) {

    suspend fun requestPatentData(): Result<List<PatentsModel>> {
        return when (val patentsResult = patentsApi.requestPatentData()) {
            is Result.Success -> Result.Success(transform(patentsResult.data.results))
            is Result.Error -> patentsResult
        }
    }

    private fun transform(patentsList: List<List<String>>) =
        patentsList.map { PatentsModel(it[0], it[1], it[2], it[3], it[5]) }
}