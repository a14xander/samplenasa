package com.sample.samplenasa.domain.usecase.fireball

import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.fireball.FireballModel
import com.sample.samplenasa.domain.repository.FireballApi

class FireballTransform(
    private val fireballRepository: FireballApi
) {

    suspend fun requestFireballData(): Result<List<FireballModel>> {
        return when (val fireballResult = fireballRepository.requestFireballData()) {
            is Result.Success -> Result.Success(transform(fireballResult.data.data))
            is Result.Error -> fireballResult
        }
    }

    private fun transform(fireballList: List<List<String?>>) =
        fireballList.map { FireballModel(it[0], it[1], it[3], it[5], it[7], it[8]) }
}