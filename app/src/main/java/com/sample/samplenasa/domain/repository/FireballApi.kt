package com.sample.samplenasa.domain.repository

import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.fireball.FireballData

interface FireballApi {
    suspend fun requestFireballData(): Result<FireballData>
}