package com.sample.samplenasa.di.modules

import com.sample.samplenasa.data.repository.apod.ApodRepository
import com.sample.samplenasa.domain.repository.ApodApi
import org.koin.dsl.module

val apodRepositoryModule = module {
    single<ApodApi> { ApodRepository(get()) }
}