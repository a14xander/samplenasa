package com.sample.samplenasa.di.scopes

import com.sample.samplenasa.ui.screens.apod.ApodFragment
import com.sample.samplenasa.ui.screens.apod.ApodViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val apodModule = module {
    scope(named<ApodFragment>()) {
        viewModel { ApodViewModel(get()) }
    }
}