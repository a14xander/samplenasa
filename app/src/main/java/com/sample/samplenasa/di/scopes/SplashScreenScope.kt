package com.sample.samplenasa.di.scopes

import com.sample.samplenasa.ui.screens.splash.SplashScreenFragment
import com.sample.samplenasa.ui.screens.splash.SplashScreenViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val splashScreenModule = module {
    scope(named<SplashScreenFragment>()) {
        viewModel { SplashScreenViewModel() }
    }
}