package com.sample.samplenasa.di.modules

import com.sample.samplenasa.data.repository.fireball.FireballRepository
import com.sample.samplenasa.domain.repository.FireballApi
import org.koin.dsl.module

val fireballRepositoryModule = module {
    single<FireballApi> { FireballRepository(get()) }
}