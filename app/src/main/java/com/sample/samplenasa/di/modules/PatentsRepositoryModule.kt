package com.sample.samplenasa.di.modules

import com.sample.samplenasa.data.repository.patents.PatentsRepository
import com.sample.samplenasa.domain.repository.PatentsApi
import org.koin.dsl.module

val patentsRepositoryModule = module {
    single<PatentsApi> { PatentsRepository(get()) }
}