package com.sample.samplenasa.di.modules

import com.google.gson.Gson
import com.sample.samplenasa.data.datasource.retrofit.JetPropulsionApi
import com.sample.samplenasa.data.datasource.retrofit.NasaApi
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitModule = module {
    single { Gson() }
    single {
        OkHttpClient
            .Builder()
            .build()
    }
    single<NasaApi> {
        getRetrofitObject(NasaApi.NASA_BASE_URL, get(), get())
            .create(NasaApi::class.java)
    }
    single<JetPropulsionApi> {
        getRetrofitObject(JetPropulsionApi.FIREBALL_BASE_URL, get(), get())
            .create(JetPropulsionApi::class.java)
    }
}

private fun getRetrofitObject(baseUrl: String, okHttpClient: OkHttpClient, gson: Gson) =
    Retrofit
        .Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
