package com.sample.samplenasa.di.scopes

import com.sample.samplenasa.domain.usecase.patents.PatentsTransform
import com.sample.samplenasa.ui.screens.patents.PatentsFragment
import com.sample.samplenasa.ui.screens.patents.PatentsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val patentsModule = module {
    scope<PatentsFragment> {
        viewModel { PatentsViewModel(get()) }
        scoped { PatentsTransform(get()) }
    }
}