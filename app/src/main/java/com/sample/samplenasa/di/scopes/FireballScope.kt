package com.sample.samplenasa.di.scopes

import com.sample.samplenasa.domain.usecase.fireball.FireballTransform
import com.sample.samplenasa.ui.screens.fireball.FireballFragment
import com.sample.samplenasa.ui.screens.fireball.FireballViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val fireballModule = module {
    scope<FireballFragment> {
        viewModel { FireballViewModel(get()) }
        scoped { FireballTransform(get()) }
    }
}