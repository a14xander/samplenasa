package com.sample.samplenasa.di.scopes

import com.sample.samplenasa.ui.screens.info.InfoFragment
import com.sample.samplenasa.ui.screens.info.InfoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val infoModule = module {
    scope<InfoFragment> {
        viewModel { InfoViewModel() }
    }
}