package com.sample.samplenasa

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.sample.samplenasa.di.modules.apodRepositoryModule
import com.sample.samplenasa.di.modules.fireballRepositoryModule
import com.sample.samplenasa.di.modules.patentsRepositoryModule
import com.sample.samplenasa.di.modules.retrofitModule
import com.sample.samplenasa.di.scopes.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class NasaApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@NasaApp)
            modules(
                splashScreenModule, apodModule, retrofitModule,
                apodRepositoryModule, fireballRepositoryModule, fireballModule,
                patentsModule, patentsRepositoryModule, infoModule
            )
        }

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}