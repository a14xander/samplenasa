package com.sample.samplenasa.data.model.responses.error

data class ErrorResponse(
    val code: String? = null,
    val message: String? = null
)
