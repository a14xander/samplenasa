package com.sample.samplenasa.data.repository

import retrofit2.Response
import java.io.IOException
import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.error.ErrorObj
import com.sample.samplenasa.data.model.responses.error.ErrorResponse

abstract class BaseRepository {
    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>) = safeApiResult(call)

    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>): Result<T> {
        return try {
            val response = call.invoke()
            if (response.isSuccessful) {
                Result.Success(response.body()!!)
            } else {
                Result.Error(ErrorObj(ErrorResponse(response.code().toString(), response.message())))
            }
        } catch (e: IOException) {
            Result.Error(ErrorObj(ErrorResponse("-1", e.message)))
        }
    }
}