package com.sample.samplenasa.data.model.core

import com.sample.samplenasa.data.model.responses.error.ErrorObj

sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val errorObj: ErrorObj) : Result<Nothing>()
}
