package com.sample.samplenasa.data.model.responses.fireball

data class FireballData(
    val data: List<List<String?>>
)