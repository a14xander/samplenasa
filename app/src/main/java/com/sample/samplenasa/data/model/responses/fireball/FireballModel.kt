package com.sample.samplenasa.data.model.responses.fireball

data class FireballModel(
    val date: String? = null,
    val energy: String? = null,
    val lat: String? = null,
    val lon: String? = null,
    val alt: String? = null,
    val vel: String? = null
)
