package com.sample.samplenasa.data.model.responses.apod

import com.sample.samplenasa.utils.*

data class Apod(
    val copyright: String? = null,
    val date: String? = null,
    val explanation: String? = null,
    val hdurl: String? = null,
    val media_type: String? = null,
    val service_version: String? = null,
    val title: String? = null,
    val url: String? = null,
    val thumbnail_url: String? = null
) : IApodType {

    override fun getItemType() =
        when (media_type) {
            IMAGE -> TYPE_IMAGE
            VIDEO -> TYPE_VIDEO
            else -> TYPE_UNKNOWN
        }
}