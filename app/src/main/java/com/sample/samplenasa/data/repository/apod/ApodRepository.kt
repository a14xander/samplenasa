package com.sample.samplenasa.data.repository.apod

import com.sample.samplenasa.data.datasource.retrofit.NasaApi
import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.requests.apod.ApodRequest
import com.sample.samplenasa.data.model.responses.apod.Apod
import com.sample.samplenasa.data.repository.BaseRepository
import com.sample.samplenasa.domain.repository.ApodApi

class ApodRepository(
    private val nasaApi: NasaApi
) : BaseRepository(), ApodApi {
    override suspend fun requestBatchApods(apodRequest: ApodRequest): Result<List<Apod>> {
        return safeApiCall { nasaApi.getApod(apodRequest.start_date, apodRequest.end_date) }
    }
}