package com.sample.samplenasa.data.datasource.retrofit

import com.sample.samplenasa.data.model.responses.apod.Apod
import com.sample.samplenasa.data.model.responses.patents.PatentsData
import com.sample.samplenasa.utils.NASA_API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NasaApi {

    @GET("planetary/apod")
    suspend fun getApod(
        @Query("start_date") start_date: String = "",
        @Query("end_date") end_date: String = "",
        @Query("thumbs") thumbs: Boolean = true,
        @Query("api_key") api_key: String = NASA_API_KEY
    ): Response<List<Apod>>

    @GET("techtransfer/patent/")
    suspend fun getPatentsData(
        @Query("api_key") api_key: String = NASA_API_KEY
    ): Response<PatentsData>

    companion object {
        const val NASA_BASE_URL = "https://api.nasa.gov/"
    }
}