package com.sample.samplenasa.data.model.requests.apod

data class ApodRequest(
    var start_date: String = "",
    var end_date: String = "",
    private val thumbs: Boolean = true
)