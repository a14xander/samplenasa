package com.sample.samplenasa.data.datasource.retrofit

import com.sample.samplenasa.data.model.responses.fireball.FireballData
import retrofit2.Response
import retrofit2.http.GET

interface JetPropulsionApi {

    @GET("fireball.api")
    suspend fun getFireballData(): Response<FireballData>

    companion object {
        const val FIREBALL_BASE_URL = "https://ssd-api.jpl.nasa.gov/"
    }
}