package com.sample.samplenasa.data.model.responses.error

data class ErrorObj(
    val error: ErrorResponse? = null
)
