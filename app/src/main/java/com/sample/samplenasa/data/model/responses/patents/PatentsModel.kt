package com.sample.samplenasa.data.model.responses.patents

data class PatentsModel(
    val id: String? = null,
    val model: String? = null,
    val title: String? = null,
    val description: String? = null,
    val scope: String? = null
)
