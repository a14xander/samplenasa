package com.sample.samplenasa.data.repository.fireball

import com.sample.samplenasa.data.datasource.retrofit.JetPropulsionApi
import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.fireball.FireballData
import com.sample.samplenasa.data.repository.BaseRepository
import com.sample.samplenasa.domain.repository.FireballApi

class FireballRepository(
    private val jetPropulsionApi: JetPropulsionApi
): BaseRepository(), FireballApi {
    override suspend fun requestFireballData(): Result<FireballData> {
        return safeApiCall { jetPropulsionApi.getFireballData() }
    }

}