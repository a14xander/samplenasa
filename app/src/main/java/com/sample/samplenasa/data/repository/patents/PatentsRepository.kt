package com.sample.samplenasa.data.repository.patents

import com.sample.samplenasa.data.datasource.retrofit.NasaApi
import com.sample.samplenasa.data.model.core.Result
import com.sample.samplenasa.data.model.responses.patents.PatentsData
import com.sample.samplenasa.data.repository.BaseRepository
import com.sample.samplenasa.domain.repository.PatentsApi

class PatentsRepository(
    private val nasaApi: NasaApi
) : BaseRepository(), PatentsApi {
    override suspend fun requestPatentData(): Result<PatentsData> {
        return safeApiCall { nasaApi.getPatentsData() }
    }
}