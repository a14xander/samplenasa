package com.sample.samplenasa.data.model.responses.patents

data class PatentsData(
    val results: List<List<String>> = emptyList()
)
